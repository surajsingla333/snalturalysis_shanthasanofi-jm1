/* eslint-disable  func-names */
/* eslint-disable  no-console */

const Alexa = require('ask-sdk');

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

const GetNewFactHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return request.type === 'LaunchRequest'
      || (request.type === 'IntentRequest'
        && request.intent.name === 'askShantha');
  },
  handle(handlerInput) {
    var courseId = handlerInput.requestEnvelope.request.intent.slots.courseId.value;
    var date = handlerInput.requestEnvelope.request.intent.slots.courseId.date;
    var speakOutput = "Attendance for " + date + "is";
    var attendance = getRandomInt(3);    speakOutput = speakOutput + attendance;


    const speechOutput = speakOutput;  

    var cardTitle = 'askShanthaBiotechnics';
    var cardContent = "Attendance Status";
    return handlerInput.responseBuilder
      .speak(speechOutput)
      .withSimpleCard( cardTitle, cardContent)
      .getResponse();
  },
};

const HelpHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return request.type === 'IntentRequest'
      && request.intent.name === 'AMAZON.HelpIntent';
  },
  handle(handlerInput) {
    var cardTitle = 'askShanthaBiotechnics';
    return handlerInput.responseBuilder
      .speak(HELP_MESSAGE)
      .reprompt(HELP_REPROMPT)
      .withSimpleCard( cardTitle, cardContent)
      .getResponse();
  },
};

const ExitHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return request.type === 'IntentRequest'
      && (request.intent.name === 'AMAZON.CancelIntent'
        || request.intent.name === 'AMAZON.StopIntent');
  },
  handle(handlerInput) {
    var cardTitle = 'Bye!';
    var cardContent = "See you next time";
    return handlerInput.responseBuilder
      .speak(STOP_MESSAGE)
      .withSimpleCard( cardTitle, cardContent)
      .getResponse();
  },
};

const SessionEndedRequestHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return request.type === 'SessionEndedRequest';
  },
  handle(handlerInput) {
    var cardTitle = 'askShanthaBiotechnics says.';
    var cardContent = 'GoodBye!';
    console.log(`Session ended with reason: ${handlerInput.requestEnvelope.request.reason}`);
    return handlerInput.responseBuilder
      .speak('bye!')
      .withShouldEndSession(true)
      .withSimpleCard( cardTitle, cardContent)
      .getResponse();
  },
};

const ErrorHandler = {
  canHandle() {
    return true;
  },
  handle(handlerInput, error) {
    console.log(`Error handled: ${error.message}`);
    var cardTitle = 'askShanthaBiotechnics';
    var cardContent = ' askShanthaBiotechnics ';
    return handlerInput.responseBuilder
      .speak('askShanthaBiotechnics')
      .withShouldEndSession(true)
      .withSimpleCard( cardTitle, cardContent)
      // .reprompt('Sorry, an error occurred.')
      .getResponse();
  },
};

const SKILL_NAME = 'askShanthaBiotechnics';
const HELP_MESSAGE = 'What can I help you with?';
const HELP_REPROMPT = 'What can I help you with?';
const STOP_MESSAGE = 'Goodbye!';

const skillBuilder = Alexa.SkillBuilders.standard();

exports.handler = skillBuilder
  .addRequestHandlers(
    GetNewFactHandler,
    HelpHandler,
    ExitHandler,
    SessionEndedRequestHandler
  )
  .addErrorHandlers(ErrorHandler)
  .lambda();
