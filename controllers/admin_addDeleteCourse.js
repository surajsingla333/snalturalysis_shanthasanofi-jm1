var express=require("express");
var app = express();
var Cryptr = require('cryptr');
cryptr = new Cryptr('myTotalySecretKey');
var path = require("path"),
    fs = require("fs");
 
var connection = require('../models/db');

module.exports.adminAddDelCor=function(req,res){
    res.sendFile(path.join(__dirname + "../../views/html" + "courseManagement.html"));  
    console.log("GET /adminAddDelCor");
}

module.exports.admin_addCourse=function(req,res){
    var course_name=req.body.courseName;
    var courseid=req.body.cid;
    var trainerid=req.body.trainerID;
    console.log(course_name);
    console.log(courseid);
    console.log(req.body.cid);
    console.log(trainerid);
    var sql = 'INSERT INTO course_list (course_id, course_name, course_trainer_id) VALUES ('+courseid+', "'+course_name+'", '+trainerid+')'; 
    connection.query(sql, function (error, results, fields) {
        if (error) {
          res.json({
              status:false,
              message:'there are some error with query'
          })
          res.render("/admin_add_del")
        }else{
            res.json({
              status:true,
              data:results,
              message:'course Added'
          })
          res.redirect('/admin_add_del')
        }
      });
  }

  module.exports.admin_deleteCourse=function(req,res){
    var course_id=req.body.corid;
    var course_name=req.body.corname;
    console.log(course_name);
    console.log(course_id);
    var sql = 'DELETE FROM course_list WHERE course_name="'+course_name+'" AND course_id='+course_id; 
    connection.query(sql, function (error, results, fields) {
        if (error) {
          res.json({
              status:false,
              message:'there are some error with query'
          })
        }else{
            res.json({
              status:true,
              data:results,
              message:'course Deleted'
          })
        }
      });
  }