var express=require("express");
var bodyParser=require('body-parser');
var app = express();
var upload = require("express-fileupload");
var session = require('express-session')
var router = express.Router();
var nodemailer = require("nodemailer");

var smtpTransport = nodemailer.createTransport({
   service: "gmail",
   host: "smtp.gmail.com",
   auth: {
       user: "chakshujain1997@gmail.com",
       pass: "c++c@0003"
   }
});
/*------------------SMTP Over-----------------------------*/

/*------------------Routing Started ------------------------*/

app.get('/sendEmail',function(req,res){
   res.sendFile('./views/html/slide9.html');
});

app.post('/sendEmail',function(req,res){
   console.log(req.file);
   // var to = req.body.empid;
   // var mailOptions={
      //  to : to,
      //  subject : req.body.courseid,
      //  text : req.body.message
   // }
   // console.log(mailOptions.to);
   // console.log(mailOptions.text);
   // console.log(mailOptions.subject);
   // console.log(mailOptions);
   // smtpTransport.sendMail(mailOptions, function(error, response){
   //  if(error){
   //     console.log(error);
   //     res.end("error");
   //  }else{
   //     console.log("Message sent: " + response.message);
   //     res.end("sent");
   //      }
// });
});
 
var registerController=require('./controllers/register');
var addCourseController=require('./controllers/addCourse')
// var initial = require('./controllers/firstPage')

var employeeSelectionControllet = require('./controllers/selection_employee')

// app.get('/selectEmployee',employeeSelectionControllet.selectEmployee);

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(upload());

// app.get('/addData', function (req, res) {  
   // res.sendFile( __dirname + "/" + "index.html" );  
   // console.log("GET /");
// })  


// login authentication
var authenticateController=require('./controllers/authenticate');
app.get('/authenticate', authenticateController.login);
app.post('/api/authenticate',authenticateController.authenticate);
app.post('/controllers/authenticate', authenticateController.authenticate);

// dashboard
var dashboard = require('./controllers/dashboard')
app.get('/dashboard', dashboard.dashboard);
// app.get('/', initial.firstPage);

// uploading
var addExcel=require('./controllers/uploadXL')
app.get('/addData', addExcel.uploadFile);
app.post('/addData', addExcel.uploaded);

// CourseUpload Admin
var adminAddDeleteCourseController=require('./controllers/admin_addDeleteCourse')
app.get('/courseManagement', adminAddDeleteCourseController.adminAddDelCor);
app.post('/api/adminaddCourse',adminAddDeleteCourseController.admin_addCourse);
app.post('/controllers/adminaddCourse',adminAddDeleteCourseController.admin_addCourse);
app.post('/api/admindeleteCourse',adminAddDeleteCourseController.admin_deleteCourse);
app.post('/controllers/admindeleteCourse',adminAddDeleteCourseController.admin_deleteCourse);

// EmployeeUpload Admin
var adminAdd_updateEmployeeController=require('./controllers/admin_AddEmployee')
app.get('/addEmployee', adminAdd_updateEmployeeController.adminAddEmployee);
app.post('/api/addEmployee',adminAdd_updateEmployeeController.addEmployee);
app.post('/controllers/addEmployee',adminAdd_updateEmployeeController.addEmployee);

// Update Employee Admin
app.get('/updateEmployee', adminAdd_updateEmployeeController.adminAddEmployee);
app.put('/controllers/updateEmployee',adminAdd_updateEmployeeController.updateEmployee);
app.put('/api/updateEmployee',adminAdd_updateEmployeeController.updateEmployee);

// EmployeeDelete Admin  
var adminDeleteEmployeeController=require('./controllers/admin_DeleteEmployee');
app.get('/deleteEmployee', adminDeleteEmployeeController.adminDelEmployee)
app.post('/api/getIdEmployee',adminDeleteEmployeeController.getDeleteEmployee);
app.post('/api/deleteEmployee',adminDeleteEmployeeController.deleteEmployee);
app.post('/controllers/getIdEmployee',adminDeleteEmployeeController.getDeleteEmployee);
app.post('/controllers/deleteEmployee',adminDeleteEmployeeController.deleteEmployee);

// Trainer Add Delete Admin
var adminAddDeleteTrainerController=require('./controllers/admin_AddDeleteTrainer')
app.get('/trainerManagement', adminAddDeleteTrainerController.adDelTrainer);


//Training Course Progress
var traineeCourseProgressController=require('./controllers/traineeCourseProgress')
app.get('/trainingProgress', traineeCourseProgressController.getCourse);
app.post('/api/traineeCourseProgress',traineeCourseProgressController.traineeCourseProgress);
app.post('/controllers/traineeCourseProgress', traineeCourseProgressController.traineeCourseProgress);
// Comp Detl
app.post('/api/traineeCourseCompleteDetails',traineeCourseProgressController.traineeCourseCompleteDetails);
app.post('/controllers/traineeCourseCompleteDetails', traineeCourseProgressController.traineeCourseCompleteDetails);


// app.post('/', function(req,res){
//    if(req.files){
//       console.log(req.files);
//       var file = req.files.filename,
//       filename = file.name;
//       file.mv("./upload"+filename,function(err){
//          if(err){
//             console.log(err);
//             res.send("Error Occured");
//          } else {
//             res.send("File Uploaded");
//          }
//       })
//    } 
// })
 
app.get('/login', function (req, res) {  
   //res.sendFile( __dirname + "/" + "login.html" );  
   console.log("GET /login");
})  
 
/* route to handle login and registration */
app.post('/api/register',registerController.register);
app.post('/api/addCourse',addCourseController.addCourse);
app.post('/api/addTrainer',adminAddDeleteTrainerController.addTrainer);
app.post('/api/deleteTrainer',adminAddDeleteTrainerController.deleteTrainer);
// app.post('/api/uploadData',addExcel.uploadData);
 
// console.log(authenticateController);
app.post('/controllers/register', registerController.register);
app.post('/controllers/addCourse', addCourseController.addCourse);
app.post('/controllers/addTrainer',adminAddDeleteTrainerController.addTrainer);
app.post('/controllers/deleteTrainer',adminAddDeleteTrainerController.deleteTrainer);
// app.post('/controllers/uploadData',addExcel.uploadData);


app.listen(3000);